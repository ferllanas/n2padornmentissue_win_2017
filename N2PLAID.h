//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa S.A de C.V
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2012 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __N2PLAID_h__
#define __N2PLAID_h__

#include "SDKDef.h"

// Company:
#define kN2PLACompanyKey	"INTERLASA"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kN2PLACompanyValue	"INTERLASA"	// Company name displayed externally.

// Plug-in:
#define kN2PLAPluginName	"N2PLockAdornament"			// Name of this plug-in.
#define kN2PLAPrefixNumber	0x96E00 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kN2PLAVersion		"1.99.0 CC 2017"						// Version of this plug-in (for the About Box).
#define kN2PLAAuthor		"Interlasa S.A de C.V"					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kN2PLAPrefixNumber above to modify the prefix.)
#define kN2PLAPrefix		RezLong(kN2PLAPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kN2PLAStringPrefix	SDK_DEF_STRINGIZE(kN2PLAPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kN2PLAMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kN2PLAMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kN2PLAPluginID, kN2PLAPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kFrmLblScriptProviderBoss, kN2PLAPrefix + 1)
DECLARE_PMID(kClassIDSpace, kFrmLblCmdBoss, kN2PLAPrefix + 2)
DECLARE_PMID(kClassIDSpace, kFrmLblPrintPrefsCmdBoss, kN2PLAPrefix + 3)
DECLARE_PMID(kClassIDSpace, kFrmLblPrefsScriptProviderBoss, kN2PLAPrefix + 4)
DECLARE_PMID(kClassIDSpace, kFrmLblErrorStringServiceBoss, kN2PLAPrefix + 5)
DECLARE_PMID(kClassIDSpace, kFrmLblAdornmentBoss, kN2PLAPrefix + 6)
DECLARE_PMID(kClassIDSpace, kFrmLblConversionProviderBoss, kN2PLAPrefix + 7)
DECLARE_PMID(kClassIDSpace, kFrmLblNewPIResponderBoss, kN2PLAPrefix + 8)
DECLARE_PMID(kClassIDSpace, kFrmLblNewDocResponderBoss, kN2PLAPrefix + 9)

// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IFRMLBLDATASUITE, kN2PLAPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IFRMLBLDATA,  kN2PLAPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IFRMLBLPRINTBOOLDATA, kN2PLAPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_FRMLBLDATAFACADE, kN2PLAPrefix + 3)

// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kFrmLblScriptProviderImpl, kN2PLAPrefix + 0 )
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataSuiteASBImpl, kN2PLAPrefix + 1 )
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataSuiteLayoutCSBImpl, kN2PLAPrefix + 2 )
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataSuiteTextCSBImpl, kN2PLAPrefix + 3 )
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataImpl, kN2PLAPrefix + 4 )
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataSuiteDefaultsCSBImpl, kN2PLAPrefix + 5 )
DECLARE_PMID(kImplementationIDSpace, kFrmLblAdornmentImpl, kN2PLAPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kFrmLblCmdImpl, kN2PLAPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataSuiteTableCSBImpl, kN2PLAPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kFrmLblPrintPrefsDataPersistImpl, kN2PLAPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kFrmLblPrintPrefsCmdImpl, kN2PLAPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kFrmLblPrefsScriptProviderImpl, kN2PLAPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kFrmLblResponderImpl, kN2PLAPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kFrmLblDataFacadeImpl, kN2PLAPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kFrmLblErrorStringServiceImpl, kN2PLAPrefix + 14)

//Script Element IDs
//Events

// Properties

// ScriptInfoIDs:
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblStringElement, kN2PLAPrefix + 0)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblSizeElement, kN2PLAPrefix + 1)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblVisibilityElement, kN2PLAPrefix + 2)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblFontColorElement, kN2PLAPrefix + 3)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblPositionEnumElement, kN2PLAPrefix + 4)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblPositionElement, kN2PLAPrefix + 5)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblPrintPrefElement, kN2PLAPrefix + 6)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblPrefObjectScriptElement, kN2PLAPrefix + 7)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblPrefObjectPropertyScriptElement, kN2PLAPrefix + 8)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblPrintPrefObjectScriptElement, kN2PLAPrefix + 9)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblPrintPrefObjectPropertyScriptElement, kN2PLAPrefix + 10)

DECLARE_PMID(kScriptInfoIDSpace, kFrmLblDisplayFranjaElement, kN2PLAPrefix + 11)
DECLARE_PMID(kScriptInfoIDSpace, kFrmLblDisplayFranjaEnumElement, kN2PLAPrefix + 12)


// ErrorIDs:
DECLARE_PMID(kErrorIDSpace, kFrmLblFailureErrorCode,			kN2PLAPrefix + 0)
DECLARE_PMID(kErrorIDSpace, kFrmLblLabelCommandFailedErrorCode,	kN2PLAPrefix + 1)
DECLARE_PMID(kErrorIDSpace, kFrmLblNoValidPageItemsSelectedErrorCode, kN2PLAPrefix + 2)

// Other StringKeys:
#define kFrmLblAboutBoxStringKey		kN2PLAStringPrefix "kFrmLblAboutBoxStringKey"
#define kFrmLblTargetMenuPath			kFrmLblPluginsMenuPath
#define kFrmLblCmdStringKey				kN2PLAStringPrefix "kFrmLblCmdStringKey"
#define kFrmLblPrintPrefsCmdStringKey	kN2PLAStringPrefix "kFrmLblPrintPrefsCmdStringKey"

#define kFrmLblFailureErrorCodeStringKey		kN2PLAStringPrefix "kFrmLblFailureErrorCodeStringKey"
#define kFrmLblLabelCommandFailedErrorKey		kN2PLAStringPrefix "kFrmLblLabelCommandFailedErrorKey"

// Initial data format version numbers: the frame label persistent data was first introduced in InDesign 1.0
#define kFrmLblInitialMajorFormat		kSDKDef_10_PersistMajorVersionNumber
#define kFrmLblInitialMinorFormat		kSDKDef_10_PersistMinorVersionNumber

// Data format version numbers used for FrmLbl InDesign 1.5.
// (No data format changes, but a forced format version number change.)
#define kFrmLbl_15_MajorFormat			kSDKDef_15_PersistMajorVersionNumber
#define kFrmLbl_15_MinorFormat			kSDKDef_15_PersistMinorVersionNumber

// Data format IDs used for FrmLbl InDesign 1.0J.
// (No data format changes, but a forced format version number change.)
#define kFrmLbl_1J_MajorFormat			kSDKDef_1J_PersistMajorVersionNumber
#define kFrmLbl_1J_MinorFormat			kSDKDef_1J_PersistMinorVersionNumber

// Note: There have been no data format changes since InDesign 2.0.

// InDesign CS4 format changes
#define kFrmLblStoreWideStringMajorFormat		kSDKDef_50_PersistMajorVersionNumber
#define kFrmLblStoreWideStringMinorFormat		RezLong(1)

// Format IDs for the PluginVersion resource
#define kFrmLblLastMajorFormatChange	kFrmLblStoreWideStringMajorFormat		// Most recent major format change
#define kFrmLblLastMinorFormatChange	kFrmLblStoreWideStringMinorFormat					// Most recent minor format change
/**/

// Schema field IDs:
#define kFrmLblLabel 0
#define kFrmLblLabelWidth 1
#define kFrmLblPointSize 2
#define kFrmLblVisibility 3
#define kFrmLblColor 4
#define kFrmLblPosition 5
#define kFrmLblDisplayFranja 6

// other constants
#define kFrmLblDefaultLabel ""
#define kFrmLblDefaultWidth 0
#define kFrmLblDefaultPointSize 12
#define kFrmLblDefaultVisibility kFalse
#define kFrmLblDefaultColor kInvalidUID
#define kFrmLblDefaultDisplayFranja kFalse
#define kFrmLblDefaultPosition 1// "About Plug-ins" sub-menu:

#define kN2PLAAboutMenuKey			kN2PLAStringPrefix "kN2PLAAboutMenuKey"
#define kN2PLAAboutMenuPath		kSDKDefStandardAboutMenuPath kN2PLACompanyKey

// "Plug-ins" sub-menu:
#define kN2PLAPluginsMenuKey 		kN2PLAStringPrefix "kN2PLAPluginsMenuKey"
#define kN2PLAPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kN2PLACompanyKey kSDKDefDelimitMenuPath kN2PLAPluginsMenuKey

// Menu item keys:

// Other StringKeys:
#define kN2PLAAboutBoxStringKey	kN2PLAStringPrefix "kN2PLAAboutBoxStringKey"
#define kN2PLATargetMenuPath kN2PLAPluginsMenuPath

// Menu item positions:


// Initial data format version numbers
#define kN2PLAFirstMajorFormatNumber  RezLong(1)
#define kN2PLAFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource
#define kN2PLACurrentMajorFormatNumber kN2PLAFirstMajorFormatNumber
#define kN2PLACurrentMinorFormatNumber kN2PLAFirstMinorFormatNumber

#endif // __N2PLAID_h__

//  Code generated by DollyXs code generator
